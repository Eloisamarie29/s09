package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication

@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	ArrayList<String> enrollees = new ArrayList<>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user", defaultValue = "John")String user){
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam("name")String name, @RequestParam("age")String age) {
		return String.format("Hello %s!, My age is %s.", name,age);
	}

	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees() {
		return enrollees;
	}

	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id) {
		String course = "";
		switch (id) {
			case "java101":
				course = "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000";
				break;
			case "sql101":
				course = "Name: SQL 101, Schedule: TTH 8:00 AM - 11:00 AM, Price: PHP 2500";
				break;
			case "javaee101":
				course = "Name: JavaEE 101, Schedule: THF 1:30 PM - 4:00 PM, Price: PHP 3500";
				break;
			default:
				course = "Course cannot be found";
				break;
		}
		return course;
	}
}

